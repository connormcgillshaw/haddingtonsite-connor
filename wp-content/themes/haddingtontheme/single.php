<?php get_header(); ?>


<section id="section2">

<div class="container">
  
  	<div class="row">

	<div class="col-md-10 col-md-offset-1" style="margin-top:100px; margin-bottom: 100px;">
    
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    
	<div class="container greytainer business-featured" style="color:#000;">
    <div class="col-md-5">
		<img class="featured-image" style="height:auto" src="<?php the_post_thumbnail( 'page-feature' ); ?>
        
        <?php echo get_post_meta($post->ID, 'map', true); ?>
        
        </div>
        <div class="col-md-7 interesttxt">
		<h1 class="sarah-txt" style="color:#005d29; margin-top:0px;"><?php the_title() ?></h1>
		    <hr>
     		<?php the_content() ?>
        </div>
	</div>
	<?php endwhile; else: ?>
      <p>
        <?php _e('Sorry, there are no posts.'); ?>
      </p>
      <?php endif; ?>
    
    
    </div>
    
    </div>
    
  </div>

</section>

<?php get_footer(); ?>