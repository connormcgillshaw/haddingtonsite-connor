 <?php
/*
Template Name: Blog Page

*/
?>

<?php get_header(); ?>


<section id="section2">

<div class="container">
  
  	<div class="row">

	<div class="col-md-12" style="margin-top:100px; margin-bottom: 100px;">
    
    <div class="grid">
					
    
    <?php


$args = array( 'posts_per_page' => 20, 'category' => 2);

$myposts = get_posts( $args );
foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
	
    
    <figure class="effect-sadie" style="width:30%">
						<img src="<?php the_post_thumbnail( 'article-thumbs' ); ?>" alt=""/>
						<figcaption>
							<h2><?php the_title(); ?></h2>
							<p><?php the_time('l, F jS, Y'); ?></p>
							<a href="<?php the_permalink(); ?>">View more</a>
						</figcaption>			
					
    
  
    
    
        </figure>
        
        
        
     </div>
    
    
    
    
<?php endforeach; 
wp_reset_postdata();?>

    
    
    </div>
    
    </div>
    
  </div>

</section>




<?php get_footer(); ?>